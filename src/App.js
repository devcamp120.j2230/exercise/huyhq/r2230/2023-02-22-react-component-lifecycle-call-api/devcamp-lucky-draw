import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import LuckyNumber from './component/LuckyNumber';

function App() {
  return (
    <div className="container">
      <LuckyNumber/>
    </div>
  );
}

export default App;
