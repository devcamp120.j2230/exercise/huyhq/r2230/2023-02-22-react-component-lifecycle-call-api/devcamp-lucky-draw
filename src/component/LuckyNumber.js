import { Component } from "react";

class LuckyNumber extends Component {
    constructor(props) {
        super(props);

        this.state = {
            number: [0, 0, 0, 0, 0, 0]
        }
    }

    getRandom = () => {
        var newNum = [];
        this.state.number.forEach(element => {
            newNum = [...newNum, Math.floor((Math.random() * 99) + 1)];
        })

        console.log(newNum);
        this.setState({
            number: newNum
        })
    }
    render() {

        return (
            <div className="text-center">
                <h3>Lucky Draw</h3>
                <ul>
                    {this.state.number.map((val, i) => {
                        return <li key={i}>{val}</li>
                    })}
                </ul>
                <button className="btn btn-primary" onClick={this.getRandom}>Click</button>
            </div>
        )
    }

}

export default LuckyNumber;